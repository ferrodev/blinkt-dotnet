using System.Collections.Generic;
using Bogus;

namespace Blinkt.Tests.Fakes.Generators
{
  public static class PixelFaker
  {
    public static Pixel GeneratePixel(bool validP = true)
    {
      return validP ? GenerateValid() : GenerateInvalid();
    }

    public static IDictionary<int, Pixel> GeneratePixels(bool validP = true)
    {
      var pixels = new Dictionary<int, Pixel>();
      for (var i = 0; i <= Blinkt.NumPixels; ++i)
      {
        pixels.Add(i, GeneratePixel(validP));
      }

      return pixels;
    }

    private static Pixel GenerateValid()
    {
      // Default all valid values to their max
      var pixel = new Faker<Pixel>()
        .RuleFor(p => p.Red, s => 255)
        .RuleFor(p => p.Green, s => 255)
        .RuleFor(p => p.Blue, s => 255)
        .RuleFor(p => p.Brightness, s => 1.0);

      return pixel.Generate();
    }

    private static Pixel GenerateInvalid()
    {
      // Default all invalid values to -1
      var pixel = new Faker<Pixel>()
        .RuleFor(p => p.Red, s => -1)
        .RuleFor(p => p.Green, s => -1)
        .RuleFor(p => p.Blue, s => -1)
        .RuleFor(p => p.Brightness, s => -1.0);

      return pixel.Generate();
    }
  }
}
