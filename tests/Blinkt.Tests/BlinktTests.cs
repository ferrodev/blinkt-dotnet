using System;
using Blinkt.Tests.Fakes.Generators;
using FluentAssertions;
using NUnit.Framework;
using Unosquare.Swan;

namespace Blinkt.Tests
{
  [TestFixture]
  public class BlinktTests
  {
    [SetUp]
    public void SetUp()
    {
      _blinkt = new Blinkt();
    }

    [TearDown]
    public void TearDown()
    {
      _blinkt = null;
    }

    private Blinkt _blinkt;

    [Test]
    public void GetPixel_WhenCalledWithValidInput_ReturnsPixel()
    {
      // arrange
      const int pixel_index = 0;

      // act
      var pixel = _blinkt.GetPixel(pixel_index);

      // assert
      pixel.Should().NotBeNull();
      pixel.Should().BeOfType<Pixel>();
    }

    [Test]
    public void GetPixel_WhenCalledWithInvalidInput_ThrowsException()
    {
      // arrange
      const int invalid_input = Blinkt.NumPixels + 1;

      // act
      Action act = () => _blinkt.GetPixel(invalid_input);

      // assert
      act.Should().Throw<ArgumentOutOfRangeException>();
    }

    [Test]
    public void SetAll_WhenCalledWithValidInputs_UpdatesPixels()
    {
      // arrange
      var expected_result = PixelFaker.GeneratePixels();
      var pixel = expected_result[0];

      // act
      _blinkt.SetAll(pixel.Red, pixel.Green, pixel.Blue, pixel.Brightness);

      // assert
      var actual_result = _blinkt.Pixels;
      actual_result.Should().BeEquivalentTo(expected_result);
    }

    [Test]
    public void SetAll_WhenCalledWithValidInputsDefaultBrightness_UpdatesPixels()
    {
      // arrange
      var expected_result = PixelFaker.GeneratePixels();
      foreach (var value in expected_result.Values)
      {
        value.Brightness = Blinkt.Brightness;
      }
      var pixel = expected_result[0];

      // act
      _blinkt.SetAll(pixel.Red, pixel.Green, pixel.Blue);

      // assert
      var actual_result = _blinkt.Pixels;
      actual_result.Should().BeEquivalentTo(expected_result);
    }

    [Test]
    public void SetAll_WhenCalledWithInvalidRedInput_ThrowsException()
    {
      // arrange
      var pixel = PixelFaker.GeneratePixel();

      // act
      Action act = () => _blinkt.SetAll(-1, pixel.Green, pixel.Blue, pixel.Brightness);

      // assert
      act.Should().Throw<ArgumentOutOfRangeException>();
    }

    [Test]
    public void SetAll_WhenCalledWithInvalidPositiveRedInput_ThrowsException()
    {
      // arrange
      var pixel = PixelFaker.GeneratePixel();

      // act
      Action act = () => _blinkt.SetAll(256, pixel.Green, pixel.Blue, pixel.Brightness);

      // assert
      act.Should().Throw<ArgumentOutOfRangeException>();
    }

    [Test]
    public void SetAll_WhenCalledWithInvalidGreenInput_ThrowsException()
    {
      // arrange
      var pixel = PixelFaker.GeneratePixel();

      // act
      Action act = () => _blinkt.SetAll(pixel.Red, -1, pixel.Blue, pixel.Brightness);

      // assert
      act.Should().Throw<ArgumentOutOfRangeException>();
    }

    [Test]
    public void SetAll_WhenCalledWithInvalidPositiveGreenInput_ThrowsException()
    {
      // arrange
      var pixel = PixelFaker.GeneratePixel();

      // act
      Action act = () => _blinkt.SetAll(pixel.Red, 256, pixel.Blue, pixel.Brightness);

      // assert
      act.Should().Throw<ArgumentOutOfRangeException>();
    }

    [Test]
    public void SetAll_WhenCalledWithInvalidBlueInput_ThrowsException()
    {
      // arrange
      var pixel = PixelFaker.GeneratePixel();

      // act
      Action act = () => _blinkt.SetAll(pixel.Red, pixel.Green, -1, pixel.Brightness);

      // assert
      act.Should().Throw<ArgumentOutOfRangeException>();
    }

    [Test]
    public void SetAll_WhenCalledWithInvalidPositiveBlueInput_ThrowsException()
    {
      // arrange
      var pixel = PixelFaker.GeneratePixel();

      // act
      Action act = () => _blinkt.SetAll(pixel.Red, pixel.Green, 256, pixel.Brightness);

      // assert
      act.Should().Throw<ArgumentOutOfRangeException>();
    }

    [Test]
    public void SetAll_WhenCalledWithInvalidBrightnessInput_ThrowsException()
    {
      // arrange
      var pixel = PixelFaker.GeneratePixel();

      // act
      Action act = () => _blinkt.SetAll(pixel.Red, pixel.Green, pixel.Blue, -1.0);

      // assert
      act.Should().Throw<ArgumentOutOfRangeException>();
    }

    [Test]
    public void SetBrightness_WhenCalledWithValidInput_UpdatesPixels()
    {
      // arrange
      const double valid_brightness = 0.5;
      var expected_values = _blinkt.Pixels.Values;
      foreach (var value in expected_values)
      {
        value.Brightness = valid_brightness;
      }

      // act
      _blinkt.SetBrightness(valid_brightness);

      // assert
      _blinkt.Pixels.Values.Should().BeEquivalentTo(expected_values);
    }

    [Test]
    public void SetBrightness_WhenCalledWithNegativeInput_ThrowsException()
    {
      // arrange
      const double invalid_brightness = -1.0;

      // act
      Action act = () => _blinkt.SetBrightness(invalid_brightness);

      // assert
      act.Should().Throw<ArgumentOutOfRangeException>();
    }

    [Test]
    public void SetBrightness_WhenCalledWithInvalidPositiveInput_ThrowsException()
    {
      // arrange
      const double invalid_brightness = 1.1;

      // act
      Action act = () => _blinkt.SetBrightness(invalid_brightness);

      // assert
      act.Should().Throw<ArgumentOutOfRangeException>();
    }
  }
}
