using Nuke.Common;
using Nuke.Common.Execution;
using Nuke.Common.Git;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.GitVersion;
using Nuke.Common.Utilities.Collections;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.IO.PathConstruction;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

[CheckBuildProjectConfigurations]
class Build : NukeBuild
{
  public static int Main () => Execute<Build>(x => x.Test);

  [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
  readonly Configuration Configuration = IsLocalBuild ? Configuration.Debug : Configuration.Release;

  [Solution] readonly Solution Solution;
  [GitRepository] readonly GitRepository GitRepository;
  [GitVersion] readonly GitVersion GitVersion;

  AbsolutePath SourceDirectory => RootDirectory / "src";
  AbsolutePath TestsDirectory => RootDirectory / "tests";
  AbsolutePath ArtifactsDirectory => RootDirectory / "artifacts";

  Target Clean => _ => _
    .Before(Restore)
    .Executes(() =>
    {
      SourceDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
      TestsDirectory.GlobDirectories("**/bin", "**/obj").ForEach(DeleteDirectory);
      EnsureCleanDirectory(ArtifactsDirectory);
    });

  Target Restore => _ => _
    .Executes(() =>
    {
      DotNetRestore(s => s
        .SetProjectFile(Solution));
    });

  Target Compile => _ => _
    .DependsOn(Restore)
    .Executes(() =>
    {
      DotNetBuild(s => s
        .SetProjectFile(Solution)
        .SetConfiguration(Configuration)
        .SetVersion(GitVersion.SemVer)
        .SetFileVersion(GitVersion.GetNormalizedFileVersion())
        .SetInformationalVersion(GitVersion.InformationalVersion)
        .EnableNoRestore());
    });

  Target Test => _ => _
    .DependsOn(Compile)
    .Executes(() =>
    {
      Solution.GetProjects("*.Tests")
        .ForEach(x => DotNetTest(s => s
          .SetProjectFile(x)
          .SetConfiguration(Configuration)
          .EnableNoBuild()
          .SetLogger("trx")
          .SetResultsDirectory(ArtifactsDirectory)));
    });
}
