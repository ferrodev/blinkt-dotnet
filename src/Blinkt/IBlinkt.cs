namespace Blinkt
{
  public interface IBlinkt
  {
    /// <summary>
    /// Clear this instance.
    /// </summary>
    void Clear();

    /// <summary>
    /// Gets the pixel.
    /// </summary>
    /// <returns>The pixel.</returns>
    /// <param name="pixelP">Pixel p.</param>
    Pixel GetPixel(int pixelP);

    /// <summary>
    /// Sets all.
    /// </summary>
    /// <param name="redP">Red p.</param>
    /// <param name="greenP">Green p.</param>
    /// <param name="blueP">Blue p.</param>
    /// <param name="brightnessP">Brightness p.</param>
    void SetAll(int redP, int greenP, int blueP, double? brightnessP = null);

    /// <summary>
    /// Sets the brightness.
    /// </summary>
    /// <param name="brightnessP">Brightness p.</param>
    void SetBrightness(double brightnessP);

    /// <summary>
    /// Sets the clear on exit.
    /// </summary>
    /// <param name="clearOnExitP">If set to <c>true</c> clear on exit p.</param>
    void SetClearOnExit(bool clearOnExitP = true);

    /// <summary>
    /// Sets the pixel.
    /// </summary>
    /// <param name="pixelP">Pixel p.</param>
    /// <param name="redP">Red p.</param>
    /// <param name="greenP">Green p.</param>
    /// <param name="blueP">Blue p.</param>
    /// <param name="brightnessP">Brightness p.</param>
    void SetPixel(int pixelP, int redP, int greenP, int blueP, double? brightnessP = null);

    /// <summary>
    /// Show this instance.
    /// </summary>
    void Show();
  }
}
