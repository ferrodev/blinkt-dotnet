namespace Blinkt
{
  public class Pixel
  {
    public int Red { get; set; }
    public int Green { get; set; }
    public int Blue { get; set; }
    public double Brightness { get; set; }
  }
}
