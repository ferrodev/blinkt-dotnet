using System;
using System.Collections.Generic;
using System.Linq;

namespace Blinkt
{
  public class Blinkt : IBlinkt
  {
    public Blinkt()
    {
      Pixels = new Dictionary<int, Pixel>();
      for (var i = 0; i <= NumPixels; ++i)
      {
        Pixels.Add(i, new Pixel  { Brightness = Brightness });
      }
    }

    public const int Dat = 23;
    public const int Clk = 24;
    public const int NumPixels = 8;
    public const double Brightness = 0.0;

    public Dictionary<int, Pixel> Pixels { get; }

    private bool _gpioSetup = false;
    private bool _clearOnExit = true;

    public void Clear()
    {
      foreach (var pixel in Pixels)
      {
        Pixels[pixel.Key] = new Pixel { Brightness = Brightness };
      }
    }

    public Pixel GetPixel(int pixelP)
    {
      if (pixelP < 0 || pixelP > NumPixels) throw new ArgumentOutOfRangeException(nameof(pixelP));

      return Pixels[pixelP];
    }

    public void SetAll(int redP, int greenP, int blueP, double? brightnessP = null)
    {
      if (redP < 0 || redP > 255) throw new ArgumentOutOfRangeException(nameof(redP));
      if (greenP < 0 || greenP > 255) throw new ArgumentOutOfRangeException(nameof(greenP));
      if (blueP < 0 || blueP > 255) throw new ArgumentOutOfRangeException(nameof(blueP));
      if (brightnessP != null)
      {
        if (brightnessP < 0 || brightnessP > 1.0) throw new ArgumentOutOfRangeException(nameof(brightnessP));
      }

      for (var i = 0; i <= NumPixels; ++i)
      {
        var old_pixel = Pixels[i];
        Pixels[i] = new Pixel
        {
          Red = redP,
          Green = greenP,
          Blue = blueP,
          Brightness = brightnessP ?? old_pixel.Brightness
        };
      }
    }

    public void SetBrightness(double brightnessP)
    {
      if (brightnessP < 0 || brightnessP > 1) throw new ArgumentOutOfRangeException(nameof(brightnessP));

      foreach (var pixel in Pixels)
      {
        pixel.Value.Brightness = brightnessP;
      }
    }

    public void SetClearOnExit(bool clearOnExitP = true)
    {
      _clearOnExit = clearOnExitP;
    }

    public void SetPixel(int pixelP, int redP, int greenP, int blueP, double? brightnessP = null)
    {
      var old_pixel = Pixels[pixelP];
      Pixels[pixelP] = new Pixel
      {
        Red = redP,
        Green = greenP,
        Blue = blueP,
        Brightness = brightnessP ?? old_pixel.Brightness
      };
    }

    public void Show()
    {
      throw new NotImplementedException();
    }

    private void Exit()
    {
      if (!_clearOnExit) return;

      Clear();
      Show();
    }

    private void WriteByte(byte byteP)
    {
      throw new NotImplementedException();
    }

    private void EOF()
    {
      throw new NotImplementedException();
    }

    private void SOF()
    {
      throw new NotImplementedException();
    }
  }
}
